<?php
    include "$racine/config/conf.php";


    $counter = 1;

    include "$racine/config/conf.php";
    
    $classesFound = false; // Variable pour suivre si des classes ont été trouvées
    
    while ($counter <= 10) { // Limiter le nombre de tentatives pour éviter une boucle infinie
        try {
            // Essayer de récupérer les données des élèves tirés au sort dans la classe correspondante
            $sql = "SELECT nom, prenom, note FROM classe$counter WHERE passage = '0'";
            $stmt = $conn->query($sql);
    
            // Vérifier si la requête a retourné des résultats
            if ($stmt !== false && $stmt->rowCount() > 0) {
                $classesFound = true; // Indiquer qu'au moins une classe a été trouvée
    
                // Afficher le titre de la classe
                echo('<h2>Classe ' . $counter . '</h2>');
    
                // Traiter les résultats
                while ($row = $stmt->fetch()) {
                    echo ('<p>' . $row[0] . ' ' . $row[1] . '</p>');
    
                    $nom = $row[0];
                    $prenom = $row[1];
                }
    
                // Incrémenter le compteur pour passer à la prochaine classe
                $counter++;
            } else {
                // Si aucune classe n'est trouvée et aucune classe n'a été trouvée auparavant, sortir de la boucle
                if (!$classesFound) {
                    break;
                } else {
                    // Si aucune classe n'est trouvée mais au moins une classe a été trouvée auparavant, continuer la boucle pour vérifier les classes suivantes
                    $counter++;
                }
            }
        } catch (PDOException $e) {
            // Gérer l'exception PDO
            // Vous pouvez choisir d'afficher un message d'erreur ou simplement ignorer cette classe et passer à la suivante
            $counter++;
        }
    }
    