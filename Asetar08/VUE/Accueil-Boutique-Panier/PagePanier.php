<!doctype html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>ASETAR08 Panier</title>

    <link rel="stylesheet" href="css.css">
    
<script>src="javascript.js"</script>

    <style>@import url('https://fonts.googleapis.com/css2?family=Hubballi&display=swap');</style>  <!--Trouvé sur google font pour changer de police de caractère-->

   
        <nav>
                <h1 style="text-align:center">  ~ ❤️ ASETAR ❤️ ~  </h1><br>
            <ul>
                <li><a href="PageAcceuil.php">Acceuil</a></li>
                <li><a href="#">Blog</a></li>
                <li class="menu-deroulant">
                <a href="PageBoutique.php">Boutique</a>
                <ul class="sous-menu">
                    <li><a href="PageBoutique.php#Sweats">Sweats</a></li>
                    <li><a href="PageBoutique.php#Mugs">Mugs</a></li>
                    <li><a href="PageBoutique.php#Coussins">Coussins</a></li>
                    <li><a href="PageBoutique.php#Pochettes">Pochettes</a></li>
                    <li><a href="PageBoutique.php#Tshirts">Tshirts</a></li>
                    <li><a href="PagePanier.php">Panier</a></li>
                </ul>
                <li><a href="login.php">Se connecter</a></li>
                <li><a href="inscription.php">S'inscrire</a></li>
                <li><a href="">Profil</a></li>
            
        </nav>
        

</head>

<body>
    <br><h4>🌸 Votre Panier 🌸</h4><br>

    <div  id="paypal-boutons"></div>

    <script src="https://www.paypal.com/sdk/js?client-id=AZkMLoYkSvDEgvfN3k04iPHcsAgeSYO4EoQTo7z18Qew2SB9xT-GpGuIqk2X4f9AnKAOL6UZ2cvWj48h"></script>

    <script src="javascript.js"> </script>
<table>
    <tr>
        <td>
            <img src="Sweat2.png">
        </td>
        <td>
            <table>
                <tr>
                    <td>
                        <input style="background-color:red; font-family: Impact, 'Arial Black'; color:white;" type="button" value="Supprimer">
                    </td>
                </tr>
                <tr>
                    <td>
                        <select style="background-color:#a73a76;">
                            <option> -- Choisir la taille -- </option>
                            <option> XS </option>
                            <option> S </option>
                            <option> M </option>
                            <option> L </option>
                            <option> XL </option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="number" value="1">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <div>
        <button id="show_popup" style="background-color:green;width: 100px;height: 35px;" type="button" onclick="paypal()">Valider la commande</button>
            <div id="popup">
                <div id="popup_content">
                    <span id="close_popup"></span><br/>
                <form method="POST">
                
                </form>
                </div>
            </div>
    </div>
</table>
<br><br>



</body>
<footer id="footer">

                <div>
                    <ul style="display: flex; margin:auto; gap: 32%;">
                        <li style="display: flex;text-shadow:3px 3px 3px black ;">
                            Conditions:
                            <a href="https://paca.dreets.gouv.fr/sites/paca.dreets.gouv.fr/IMG/pdf/cgu__demat_d5b9d.pdf"> CGU</a>
                            / 
                            <a href="https://www.legalplace.fr/guides/conditions-generales-vente/"> CGV</a>
                        </li>
                        <li style="display: flex;text-shadow:3px 3px 3px black ;">
                            Réglementation:
                             <a href="https://www.cnil.fr/fr/conformite-rgpd-information-des-personnes-et-transparence"> RGPD</a>
                        </li>
                        <li style="display: flex;text-shadow:3px 3px 3px black ;">
                            Nous contacter:  
                             <a href="https://trello.com/b/gE1eG3Mi">  Trello@issou.gouv</a>
                        </li>
                    </ul>
                </div>
                <div id="copyright">
                    © 2022 Team3 Copyright, S9, Lycée Monge. All Rights Reserved
                </div>
</footer>
</html>
