import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class CasseBrique extends JPanel implements MouseListener
    {
    private static final String TITLE = "SIO2/SLAM - rentrée 2022 - Casse-Brique";
    private static final String MES_ALPHA1 = "Transparence (alpha) supportée par le système";
    private static final String MES_ALPHA2 = "Transparence (alpha) non supportée par le système";


    private static final String PAUSE_MESSAGE = "pause !";
    private static final int LARGPANEL = 350;
    private static final int HAUTPANEL = 350;
    private static final boolean DEBUG = true; // Mode debug
    /**
     * Liens vers les objets balle et raquette
     */
    private final Raquette raquette;
    private final Balle balle;
    private final Balle balle2;
    /**
     * Vitesse de la balle au début du jeu
     */
    private int speed = 2;


    private boolean pause = true;

    /**
     * Constructeur de l'espace de jeu du mini-tenis
     */
    public CasseBrique()
        {
        this.setPreferredSize(new Dimension(CasseBrique.LARGPANEL, CasseBrique.HAUTPANEL));
        this.setDoubleBuffered(true);
        this.balle = new Balle(this, DEBUG);
        this.balle2 = new Balle(this, DEBUG);
        this.raquette = new Raquette(this, DEBUG);
        this.setBackground(Color.darkGray);
        this.addMouseListener(this);
        balle2.setx(60);
        balle2.sety(100);
        }

    /**
     * -------------------------------------------------------------------------
     * Méthode statique principale pour le lancement du jeu ...
     *
     * @param args
     *
     * @throws InterruptedException : Pour d'éventuelles erreurs liées au délai
     * -------------------------------------------------------------------------
     */
    public static void main(String[] args) throws InterruptedException
        {
        /*
         * Construction de la fenêtre ...
         */
        JFrame frame = new JFrame();
        CasseBrique game = new CasseBrique();
        game.setFocusable(true);
        frame.setTitle(CasseBrique.TITLE);
        frame.add(game);
        //frame.setSize(350, 450);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.pack();
        frame.setVisible(true);

        /*
         * Test si la config écran supporte la transparence
         */
        game.checkAlpha();

        /*
         * Boucle infinie (toutes les 10 ms)
         */
        while (true)
            {
            /*
             * Mise à jour des positions des éléments graphiques
             */
            if (!game.pause)
                {
                game.move(game.balle, game.balle2);
                }

            game.repaint();
            /*
             */
            Thread.sleep(10);
            }
        }


    /**
     * @param g Espace
     */
    @Override
    public void paint(Graphics g)
        {
        // Appel de la méthode 'paint' du parent
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                             RenderingHints.VALUE_ANTIALIAS_ON);

        //
        balle.paint(g2d);
        balle2.paint(g2d);
        balle.name("alpha");
        balle2.name("beta");
        raquette.paint(g2d);

        if (pause)
            {
            // Affichage message pause ...
            g2d.setColor(Color.LIGHT_GRAY);
            g2d.setFont(new Font("Verdana", Font.BOLD, 30));
            int x = (this.getWidth() / 2) - 80;
            int y = (this.getHeight() / 2) - 30;
            g2d.drawString(CasseBrique.PAUSE_MESSAGE, x, y);
            }
        }

    /**
     */
    private void move(Balle balle, Balle balle2)
        {
        balle.move(balle2);
        balle2.move(balle);
        }


    /**
     * Récup. de la vitesse de la balle
     *
     * @return vitesse actuelle de la balle
     */
    public int getSpeed()
        {
        return this.speed;
        }

    /**
     * Init vitesse balle
     *
     * @param s nouvelle vitesse
     */
    public void setSpeed(int s)
        {
        this.speed = 2;
        }

    /**
     * Récup. pos 'Y' raquette
     *
     * @return Pos Y raquette
     */
    public int getPosYRaquette()
        {
        return this.raquette.getY();
        }

    /**
     * Récup. pos 'x' raquette
     *
     * @return Pos x raquette
     */
    public int getPosXRaquette()
        {
        return this.raquette.getX();
        }

        public int getPosWidthRaquette()
        {
        return this.raquette.getWidth();
        }

        public int getPosHeightRaquette()
        {
        return this.raquette.getHeight();
        }

    /**
     * Récup position raquette
     *
     * @return Position raquette (coord. rect.)
     */
    public Rectangle getPositionRaquette()
        {
        return new Rectangle(this.raquette.getPositionRaquette());
        }

    /**
     * Test si l'affichage supporte la transparence
     */
    private void checkAlpha()
        {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        if (ge.getDefaultScreenDevice().isWindowTranslucencySupported(GraphicsDevice.WindowTranslucency.TRANSLUCENT))
            {
            //this.ALPHA = true;
            if (DEBUG)
                {
                System.out.println("=> " + CasseBrique.MES_ALPHA1);
                }
            }
        else if (DEBUG)
            {
            System.out.println("=> " + CasseBrique.MES_ALPHA2);
            }
        }

    @Override
    public void mouseClicked(MouseEvent e)
        {
        System.out.println("Test");
        this.pause = !this.pause;
        }

    @Override
    public void mousePressed(MouseEvent e)
        {
        }

    @Override
    public void mouseReleased(MouseEvent e)
        {
        }

    @Override
    public void mouseEntered(MouseEvent e)
        {
        }

    @Override
    public void mouseExited(MouseEvent e)
        {
        }
    }
