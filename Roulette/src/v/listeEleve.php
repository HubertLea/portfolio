<?php
echo ('
<!DOCTYPE html>
    <meta charset="UTF-8">

    <html lang="fr">
        <head>
            <title>Projet Roulette</title>

            <link rel="stylesheet" href="../../public/homePage.css">
        </head>

        <body>
            <nav class="menu">
                <ul>
                    <li><a href="\Roulette\src\v\homePage.php">Accueil</a></li>
                    <li><a href="\Roulette\src\v\listeEleve.php">Liste des élèves</a></li>
                    <li><a href="\Roulette\src\v\tirageAuSort.php">Tirage au sort</a></li>
                </ul>
            </nav>

            <h1>🏴‍☠️ Liste de nos élèves 🏴‍☠️ </h1>
            <br><br><br>

            <table>
                <tr id="ligne">
                    <td id="colone">Nom</td>
                    <td id="colone">Prénom</td>
                    <td id="colone">Note</td>
                </tr>
                
') ?>
                <?php 

                


    

                
// Informations de connexion à la base de données
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "roulette";

try {
    // Créer une instance de la classe PDO
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);

    // Définir le mode d'erreur de PDO sur Exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    // Exemple d'exécution d'une requête
    $sql = "SELECT * FROM liste";
    $stmt = $conn->query($sql);

    // Traiter les résultats
    while ($row = $stmt->fetch()) {
        echo ("<tr>");
        echo "<td>" . $row['nom']."</td>";
        echo "<td>" . $row['prenom']."</td>";
        echo "<td>" . $row['note']."</td>";
        echo ("</tr>");
    }
} catch(PDOException $e) {
    echo "Erreur de connexion : " . $e->getMessage();
}

// Fermer la connexion à la base de données
$conn = null;


?>