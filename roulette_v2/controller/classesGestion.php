<?php
$counter = 1;

echo('<CENTER>');

echo('<br><br><form method="post">
<input type="text" id="NomEleve" name="NomEleve" placeholder="Nom du nouvel élève">
<input type="text" id="PrenomEleve" name="PrenomEleve" placeholder="Prénom du nouvel élève">
<input type="submit" id="ajouterEleve" name="ajouterEleve" value="Ajouter un élève"></form>');
echo('<h2>—————~ஜ ・。・。・。・。・ ஜ~—————</h2>');

include "$racine/config/conf.php";

$classesFound = false; // Variable pour suivre si des classes ont été trouvées

while ($counter <= 10) { // Limiter le nombre de tentatives pour éviter une boucle infinie
    try {
        // Essayer de récupérer les données de la classe correspondante
        $sql = "SELECT nom, prenom, note FROM classe$counter";
        $stmt = $conn->query($sql);

        // Vérifier si la requête a retourné des résultats
        if ($stmt !== false && $stmt->rowCount() > 0) {
            $classesFound = true; // Indiquer qu'au moins une classe a été trouvée

            // Afficher le titre de la classe
            
            echo('<h3>Classe ' . $counter . '</h3>');
            echo('<table class="striped-table">');
            echo('<tr>');

            // Traiter les résultats
            while ($row = $stmt->fetch()) {
                echo('<tr>
                        <td>'. $row[0] .'</td>
                        <td>'. $row[1].'</td>
                        <td>
                            <input type="submit" id="supprimerEleve" name="supprimerEleve" value="🗑">
                            <input type="submit" id="modifierEleve" name="modifierEleve" value="🖉">
                        </td>
                    </tr>');

                $nom = $row[0];
                $prenom = $row[1];
            }

            echo('</table><br>&nbsp;&nbsp;&nbsp;<input type="submit" id="supprimerClasse" name="supprimerClasse" value="🗑"><h2>—————~ஜ ・。・。・。・。・ ஜ~—————</h2>
            ');
            

            // Incrémenter le compteur pour passer à la prochaine classe
            $counter++;
        } else {
            // Si aucune classe n'est trouvée et aucune classe n'a été trouvée auparavant, sortir de la boucle
            if (!$classesFound) {
                break;
            } else {
                // Si aucune classe n'est trouvée mais au moins une classe a été trouvée auparavant, continuer la boucle pour vérifier les classes suivantes
                $counter++;
            }
        }
    } catch (PDOException $e) {
        // Gérer l'exception PDO
        // Vous pouvez choisir d'afficher un message d'erreur ou simplement ignorer cette classe et passer à la suivante
        $counter++;
    }
}

if(isset($_POST['ajouterEleve'])){

    $classe = $_POST['classes'];
    $NomEleve = $_POST['NomEleve'];
    $PrenomEleve = $_POST['PrenomEleve'];
    
    include "$racine/config/conf.php";

    $sql22 = "INSERT INTO $classe (nom, prenom) VALUES (:nom, :prenom)";
    $stmt22 = $conn->prepare($sql22);
    $stmt22->bindParam(':nom', $NomEleve);
    $stmt22->bindParam(':prenom', $PrenomEleve);
    $stmt22->execute();

    $conn = null;
};

echo('</CENTER>');
?>