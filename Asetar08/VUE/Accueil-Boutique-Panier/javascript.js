paypal.Buttons({
    createOrder : function (data, actions) {
        var produits = [
            {
                name: "Produit 1",
                description: "Description du produit 1",
                quantity: 1,
                unit_amount : { value: 9.9, currency_code : "EUR"}
            },
            {
                name: "Produit 2",
                description: "Description du produit 2",
                quantity: 1,
                unit_amount : { value: 8.0, currency_code : "EUR"}
            }
        ];

        vartotal_amount = preoduit.reduce(function(total, product){
            return total + product.unit_amount.value * product.quantity;
        }, 0);

        return actions.order.create({
            purchase_units : [{
                items : produits,
                amount : {
                    value : total_amount,
                    currency_code : "EUR",
                    breakdown : {
                        item_total : { value : total_amount, currency_code : "EUR"}
                    }
                }
            }]
        });
    },
    onApprove : function (data, actions) {
        return actions.order.capture().then(function(details) {
            alert(details.payer.name.given_name + '' + details.payer.name.surname + ', votre transaction est effectuée. Vous allez recevoir unr notification très bientôt lors de la validation de votre paiement.');
        });
    },

    onCancel : function(data) {
        alert("Transaction annulée !");
    }
}).render("#paypal-boutons");
