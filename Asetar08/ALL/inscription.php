<?php

require_once("inc/init.inc.php");

if($_POST){
    
    $verif_caractere = preg_match('#^[a-zA-Z0-9._-]+$',$_POST['pseudo']);

    if(!$verif_caractere &&(strlen($_POST['pseudo']) < 1 || strlen($_POST['pseudo']) > 20)){
        $contenu .="<div class='erreur'>Le pseudo doit contenir entre 1 et 20 caractères.<br> Caractères accepté : Lettre de A à Z et chiffres de 0 à 9</div>";
    }
    else {
        $membre = executeRequete("SELECT * FROM membre WHERE pseudo='$_POST[pseudo]'");
        if($membre->num_rows > 0){
            $contenu ="<div class='erreur'>Pseudo indisponible. Veuillez en choisir un autre svp</div>";
        }
        else{
            $_POST['mdp'] = hash('sha256' ,"$_POST[mdp]");
            foreach($_POST as $indice => $valeur){
                $_POST[$indice] = htmlentities(addslashes($valeur));
            }
            executeRequete("INSERT INTO membre (pseudo, mdp, nom, prenom, email, civilite,{ ville, code_postal, adresse) VALUES ('$_POST[pseudo]', '$_POST[mdp]', '$_POST[nom]', '$_POST[prenom]', '$_POST[email]', '$_POST[civilite]', '$_POST[ville]', '$_POST[code_postal]', '$_POST[adresse]')");
            $contenu = "<div class='validation'>Vous êtes inscrit à notre site web. <a href='\connexion.php\'><u>Cliquez ici pour vous connecter</u></a></div>";
        }
    }
}

?>

<?php

require_once("inc/haut.inc.php");
echo $contenu;
?>

<form method="post" action="">
    <label for="pseudo"Pseudo>Pseudo</label><br><input type="text" id="pseudo" name="pseudo" maxlenght="20" placeholder="votre pseudo" pattern="[a-zA-Z0-9-_.] {1,20}" tiltle="caractères acceptés: a-zA-Z-9-_." required="required">
    <br>
    <br>
    <label for="mdp">Mot de passe</label><br><input type="password" name="mdp" id="mdp" required="required">
    <br>
    <br>
    <label for="nom">Nom</label><br><input type="text" name="nom" id="nom" placeholder="votre nom">
    <br>
    <br>
    <label for="prenom">Prénom</label><br><input type="text" name="prenom" id="prenom" placeholder="votre pénom">
    <br>
    <br>
    <label for="email">Email</label><br><input type="email" name="email" id="email" placeholder="exemple@gmail.com">
    <br>
    <br>
    <label for="civilite">Civilité</label><br><input type="radio" name="civilite" value="m" checked="">Homme<input name="civilite" value="f" type="radio">Femme
    <br>
    <br>
    <label for="ville">Ville</label><br><input type="text" name="ville" id="ville" placeholder="votre ville" pattern="[a-zA-Z0-9-_.] {1,25}" tiltle="caractères acceptés: a-zA-Z-9-_.">
    <br>
    <br>
    <label for="cp">Code Postal</label><br><input type="text" name="code_ppostal" id="code_postal" placeholder="votre code postal" pattern="[0-9] {5}" title="5 chiffres requis: 0-9">
    <br>
    <br>
    <label for="adresse">Adresse</label><br><textarea id="adresse" name="adresse" placeholder="votre adresse" pattern="[a-zA-Z0-9-_.] {5,30}" tiltle="caractères acceptés: a-zA-Z-9-_."></textarea>
    <br>
    <br>
    <input type="submit" name="inscription" value="S'inscrire">
</form>

<?php

require_once("inc/bas.inc.php");

?>

