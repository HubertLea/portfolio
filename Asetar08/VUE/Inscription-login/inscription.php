<?php

session_name(Asetar_inscription);
session_start();

include("infos.php");

$valider = $_POST["inscrire"];
$erreur = "";

if (isset($valider)) {
    if (empty($nom)) $erreur = "Le champs nom est obligatoire !";
    elseif (empty($prenom)) $erreur = "Le champs prénom est obligatoire !";
    elseif (empty($pseudo)) $erreur = "Le champs pseudo est obligatoire !";
    elseif (empty($mdp)) $erreur = "Le champs mot de passe est obligatoire !";
    elseif ($mdp != $mdpConf) $erreur = " Mot de passe différent !";
    else {
        include("connexion.php");
        $verif_pseudo = $pdo->prepare("select id from UTILISATEUR where pseudo=? limit 1");
        $verif_pseudo = $pdo->execute(array($pseudo));
        $user_pseudo = $verif_pseudo->fetchAll();
        if (count($user_pseudo) > 0)
        $erreur = "Pseudo existe déja !";
        else {
            $ins = $pdo->prepare("insert into UTILISATEUR(nom,prenom,pseudo,mdp) values(?,?,?,?)");
            if ($ins->execute(array($nom, $prenom, sha256($mdp)))) header("location:login.php");
        }
    }
}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
        <link rel="stylesheet" href="inscription.css">
    </head>
    <body>
    <h1><br><br><br><br><br><br><br><br><br><br></h1>
        <table>
            <tr>
                <td>
                    <img src="AnimeGirl3.png">
                </td>
            <tr>
                <td>
                    <div class="erreur"><?php echo $erreur ?></div>
                    <form name="fo" method="post" action="">
                        <input type="text" name="nom" placeholder="Nom" value="<?= $nom ?>" required>
                        <input type="text" name="prenom" placeholder="Prénom" value="<?= $prenom ?>" required>
                        <input type="text" name="pseudo" placeholder="Pseudo" value="<?= $pseudo ?>" required>
                        <input type="password" name="mdp" placeholder="Mot de passe" value="<?= $mdp ?>" required>
                        <input type="password" name="mdpConf" placeholder="Confirmer votre Mot de passe" required>
                        <input type="submit" name="inscrire" value="S'inscrire">
                    </form>
                </td>
            </tr>
        </table>
    </body>
</html>
