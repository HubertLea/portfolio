<?php

function executeRequete($req){
    global $bdd;
    $resultat = $bdd->query($req);
    if (!$resultat){
        die("Erreur sur la requete sql.<br>Message: ".$bdd->error . "<br>Code: ".$req);
    }
    return $resultat;
}

?>