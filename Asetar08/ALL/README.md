# ASETAR08 Team3

https://trello.com/b/gE1eG3Mi

|Actions réalisées|             Controlleur|                        Vue|                          Modèle|
|---------|--------------------------------|---------------------------|--------------------------------|
|Accueil|-|`view/PageAccueil.php`|-|
|Boutique|-|`view/PageBoutique.php`|-|
|Panier|-|`view/PagePanier.php`|-|
|Connexion|`authentification/connexion.php`|`authentfication/login.php`|`authentification/connexion.php`|
|Déconnexion|`authentification/deconnexion.php`|-|-|
|Inscription|`authentification/inscription.php`|`authentification/inscription.php`|-|
|Session|`authentification/session.php`|`authentification/session.php`|-|
|PayPal|`paypal/functionPaypal.html`|`paypal/functionPaypal.html`|-|
|Header|-|`view/header.html`|-|
|Footer|-|`view/footer.html`|-|
|Carroussel|-|`view/carroussel.html`|-|
|ProfilVue|-|`view/profil.vue.php`|-|
|PublicProfilVue|-|`view/publicProfil.vue.php`|-|
|ModifierProfilVue|-|`view/modifierProfil.vue.php`|-|

Arborescence utilisée :

<tt>

├── <b>modele</b>        
│   └── saveInfo.js  
├── <b>controller</b>    
│   ├── connexion.php     
│   ├── actionsList.php     
│   ├── mainControl.php
│   ├── deconnexion.php          
│   ├── init.php     
│   └── infos.php     
├── <b>images</b>    
│   ├── Actualite1.png     
│   ├── Coms.png     
│   ├── Coussin.png     
│   ├── Debardeur.png     
│   ├── ImageFond11.png     
│   ├── Mug1.png     
│   ├── Mug2.png     
│   ├── Pochette1.png     
│   ├── Pochette2.png     
│   ├── Sweat1.png     
│   ├── Sweat2.png     
│   ├── Sweat3.png     
│   ├── Tshirt1.png     
│   ├── Tshirt2.png     
│   ├── Tshirt3.png     
│   ├── coeurPlein.png     
│   ├── coeurVide.png     
│   ├── favorisPlein.png     
│   ├── favorisVide.png     
│   ├── logo.png     
│   ├── test.jpg     
│   ├── <b>zizou.png</b>    
│   ├── AnimeGirl3.png        
│   ├── Cat.gif       
│   ├── ImageFond22.jpg     
│   └── baniere.png     
├── <b>view</b>     
│   ├── PageAccueil.php     
│   ├── PageBoutique.php     
│   ├── inscription.php     
│   ├── PagePanier.php     
│   ├── session.php     
│   ├── header.html     
│   ├── header.css     
│   ├── inscription.css     
│   ├── login.php     
│   ├── style.css     
│   ├── login.css     
│   ├── footer.vue.html     
│   ├── footer.css     
│   ├── modifierProfil.vue.php     
│   ├── profil.css      
│   ├── profil.vue.php     
│   ├── publicProfil.vue.php     
│   ├── carroussel.html     
│   └── carroussel.css     
└── <b>index.php</b> -> <ins><i>Point d'entrée unique du site</i></ins>    

</tt>
