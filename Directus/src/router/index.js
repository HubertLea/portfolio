import { createRouter, createWebHistory } from 'vue-router';
import HomeView from '../views/PageAccueil.vue';
import PageEval from '../views/PageEval.vue';
import PageComp from '../views/PageComp.vue';
import PageEvalu from '../views/PageEvalu.vue';
import PageConexion from '../views/PageConexion.vue';
import PageTest from '../views/PageTest.vue';

const routes = [
    {
      path: '/',
      name: 'home',
      component: HomeView,
    },
    {
      path: '/PageEval',
      name: 'eval',
      component: PageEval
    },
    {
      path: '/PageConexion',
      name: 'conexion',
      component: PageConexion,
    },
    {
      path: '/PageComp',
      name: 'comp',
      component: PageComp
    },
    {
      path: '/PageEvalu',
      name: 'evalu',
      component: PageEvalu
    },
    {
      path: '/PageEval',
      name: 'Eval',
      component: PageEval
    },
    {
      path: '/PageTest',
      name: 'test',
      component: PageTest
    },
  ]


const router= createRouter ({
    history: createWebHistory(),
    routes
})

export default router;
