-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mer. 03 avr. 2024 à 11:21
-- Version du serveur : 8.2.0
-- Version de PHP : 8.2.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `db_roulette`
--

-- --------------------------------------------------------

--
-- Structure de la table `classe1`
--

DROP TABLE IF EXISTS `classe1`;
CREATE TABLE IF NOT EXISTS `classe1` (
  `id` int NOT NULL AUTO_INCREMENT,
  `Nom` text NOT NULL,
  `Prenom` text NOT NULL,
  `Classe` int NOT NULL DEFAULT '1',
  `Passage` int NOT NULL DEFAULT '0',
  `Absence` int NOT NULL DEFAULT '0',
  `Note` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `classe1`
--

INSERT INTO `classe1` (`id`, `Nom`, `Prenom`, `Classe`, `Passage`, `Absence`, `Note`) VALUES
(1, 'Hubert', 'Léa', 1, 0, 0, ''),
(2, 'Bob', 'Slague', 1, 0, 0, ''),
(3, 'Jean', 'Crapaud', 1, 0, 0, ''),
(4, 'Test', 'Orange', 1, 0, 0, ''),
(5, 'Monsieur', 'Madame', 1, 0, 0, ''),
(6, 'Hell', 'Alastor', 1, 0, 0, ''),
(9, 'Minus', 'Cule', 1, 0, 0, '');

-- --------------------------------------------------------

--
-- Structure de la table `classe2`
--

DROP TABLE IF EXISTS `classe2`;
CREATE TABLE IF NOT EXISTS `classe2` (
  `id` int NOT NULL AUTO_INCREMENT,
  `Nom` text NOT NULL,
  `Prenom` text NOT NULL,
  `Classe` int NOT NULL DEFAULT '2',
  `Passage` int NOT NULL DEFAULT '0',
  `Absence` int NOT NULL DEFAULT '0',
  `Note` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `classe2`
--

INSERT INTO `classe2` (`id`, `Nom`, `Prenom`, `Classe`, `Passage`, `Absence`, `Note`) VALUES
(1, 'Walter', 'Elias', 2, 0, 0, ''),
(2, 'Hormann', 'Aragorn', 2, 0, 0, ''),
(3, 'Vanderhyle', 'Raven', 2, 0, 0, ''),
(4, 'Asmira', 'Indhylle', 2, 0, 0, ''),
(5, 'Asthaors', 'Xerxes', 2, 0, 0, ''),
(6, 'Morgan', 'Arthur', 2, 0, 0, ''),
(9, 'Lord', 'MaxEstLa', 2, 0, 0, '');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
