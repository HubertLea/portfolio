import { createApp } from "vue";
//import { createPinia } from "pinia";

import router from './router';

import App from "./App.vue";

import "./assets/main.css";

//const app = 
createApp(App).use(router).mount("#app");

//app.use(createPinia());

//app.mount("#app");
