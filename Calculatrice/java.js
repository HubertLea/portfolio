//fonction des nombres :
function key (a)
    {   
        if ((document.calculatrice.dpp.value=="+")||(document.calculatrice.dpp.value=="-")||(document.calculatrice.dpp.value=="x")||(document.calculatrice.dpp.value=="/")||(document.calculatrice.dpp.value=="^")||(document.calculatrice.dpp.value=="cos")||(document.calculatrice.dpp.value=="sin")||(document.calculatrice.dpp.value=="tan")||(document.calculatrice.dpp.value=="√")) //si une de ses valeurs est écrite dans 'dpp' il doit écrire les chiffre dans 'dppp'
        {
            document.calculatrice.dppp.value = document.calculatrice.dppp.value + a; //concaténation 
        }
        else //si il n'y à rien d'écrit dans 'dpp' écrire dans 'dp'
        {
            document.calculatrice.dp.value = document.calculatrice.dp.value + a //concaténation
        }
    }
// supprime le contenu des input concernant les calculs
    function suppr () 
    {
        document.calculatrice.dp.value = "";
        document.calculatrice.dpp.value = "";
        document.calculatrice.dppp.value="";
        document.calculatrice.resultat.value="";
    }
//supprime le contenu de l'input concernant les formules
    function delette ()
    {
        document.calculatrice.formules.value = "";
    }
//concataine les lettres et autres outils servant dans le fait de créer des forumules (moyen mémotechnique)
    function keyy (a)
    {   
    document.calculatrice.formules.value = document.calculatrice.formules.value + a;    
    }
//écrit l'opérateur dans dpp
 function calcul (b)
    {
        document.calculatrice.dpp.value = b;   
    }
//fait en sorte de re marquer le résultat dans dp pour enchainer facilement les calculs si besoins/envie
function keepResult ()
{
    document.calculatrice.dp.value=document.calculatrice.resultat.value;
    document.calculatrice.dpp.value="";
    document.calculatrice.dppp.value="";
}
//la ou ce passe la phase de calcul, une fois le bouton pressé il déconcataine et fait le calcul pour chaque signe opératoire
function egall ()
{
    if (document.calculatrice.dpp.value=="+")
    {
        document.calculatrice.resultat.value=Number(document.calculatrice.dp.value) + Number(document.calculatrice.dppp.value);
    }
    if (document.calculatrice.dpp.value=="-")
    {
        document.calculatrice.resultat.value=Number(document.calculatrice.dp.value) - Number(document.calculatrice.dppp.value);
    }
    if (document.calculatrice.dpp.value=="x")
    {
        document.calculatrice.resultat.value=Number(document.calculatrice.dp.value) * Number(document.calculatrice.dppp.value);
    }
    if (document.calculatrice.dpp.value=="/")
    {
        document.calculatrice.resultat.value=Number(document.calculatrice.dp.value) / Number(document.calculatrice.dppp.value);
    }
    if (document.calculatrice.dpp.value=="^")
    {
        document.calculatrice.resultat.value=Number(document.calculatrice.dp.value) ** Number(document.calculatrice.dppp.value);
    }
    if (document.calculatrice.dpp.value=="cos")
    {
        document.calculatrice.resultat.value=Math.cos(Number(document.calculatrice.dp.value));
        document.calculatrice.dppp.value="rien";
    }
    if (document.calculatrice.dpp.value=="sin")
    {
        document.calculatrice.resultat.value=Math.sin(Number(document.calculatrice.dp.value));
        document.calculatrice.dppp.value="rien";
    }
    if (document.calculatrice.dpp.value=="tan")
    {
        document.calculatrice.resultat.value=Math.tan(Number(document.calculatrice.dp.value));
        document.calculatrice.dppp.value="rien";
    }
    if (document.calculatrice.dpp.value=="√")
    {
        document.calculatrice.resultat.value=Number(document.calculatrice.dp.value) * Math.sqrt(Number(document.calculatrice.dppp.value));
    }
}

/* je ne suis pas parvenue à faire fonctionner les calculs sur une seule ligne de calculs mais j'ai tout de même essayé :
var backupNb1 = "";
    var nombre2="";
    var clicAdd="";
    var clickSous="";
    var multi="";
    var division ="";

function key (a)
    {
        if ((document.calc.dp.value=="0")||(clicAdd="+"))
            {
                document.calc.dp.value = a;
                clicAdd="";
            }
        else
            {
                document.calc.dp.value = document.calc.dp.value + a;
            }

je n'ai pas pu comprendre le principe de mémoire ou opération en cours et de comment ça fonctionnais alors j'ai réaliser quelque chose que j'ai compris


licence Apache 2.0 ; HUBERT Léa ; lea.new2@orange.fr*/