<br><br><br><br>
        
        <footer>

        <link rel="stylesheet" href="resources/css/footer.css"> 

        <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-3 item">
                        <h3>Plus sur moi</h3>
                        <div id="ulfoot">
                            <li id="lifoot"><a id="afoott" class="telecharger_pdf" data-chemin="resources/pdf/CVHubertLea.pdf">Mon CV</a></li>
                            <li id="lifoot"><a id="afoot" href="https://portfoliohubertlea.000webhostapp.com/index.html">Mon portfolio</a></li>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3 item">
                        <h3>Me contacter</h3>
                        <div id="ulfoot">
                            <li id="lifoot2"><a id="afoot">Email : lea.new2@orange.fr</a></li>
                            <li id="lifoot2"><a id="afoot">Numéro de téléphone : 07.81.20.64.14</a></li>
                        </div>
                    </div>
                    <br>
                    <div class="col-md-6 item text">
                        <h3>Roulette</h3>
                        <p id="pfoot">Projet réalisé en deuxième années du BTS SIO</p>
                    </div>
                </div>
                <p id="pfoot" class="copyright">Roulette © 2023</p>
            </div>

        </footer>

        <script>
    // Sélectionnez tous les éléments avec la classe "telecharger_pdf"
    var liensPDF = document.querySelectorAll('.telecharger_pdf');

    // Pour chaque lien, ajoutez un écouteur d'événements pour le clic
    liensPDF.forEach(function(lien) {
        lien.addEventListener('click', function(e) {
            e.preventDefault(); // Empêche le comportement par défaut du lien

            // Récupérez le chemin du fichier PDF à partir de l'attribut de données
            var cheminPDF = this.getAttribute('data-chemin');

            // Créez un élément <a> temporaire
            var lienTemporaire = document.createElement('a');
            lienTemporaire.href = cheminPDF;
            lienTemporaire.target = '_blank'; // Pour ouvrir dans un nouvel onglet si nécessaire
            lienTemporaire.download = 'nom-du-fichier.pdf'; // Nom du fichier à télécharger
            document.body.appendChild(lienTemporaire);

            // Cliquez sur le lien temporaire pour déclencher le téléchargement
            lienTemporaire.click();

            // Supprimez le lien temporaire après le téléchargement
            document.body.removeChild(lienTemporaire);
        });
    });
</script>

    </body>
</html>
