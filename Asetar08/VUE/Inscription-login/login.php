<?php

session_name(Asetar_login);
session_start();
include("infos.php");

$valider = $_POST["valider"];
$erreur = "";

if (isset($valider)) {
    include("connexion.php");
    $verif = $pdo->prepare("select * from UTILISATEUR where pseudo=? and mdp=? limit 1");
    $verif->execute(array($pseudo, $mdp_crypt));
    $user = $verif->fetchAll();
    if (count($user) > 0) {
        $_SESSION["prenom_nom"] = ucfirst (strtolower($user[0]["prenom"])) . " " . strtoupper($user[0]["nom"]);
        $_SESSION["connecter"] = "yes";
        header("location:session.php");
    }
    else {
    $erreur = " Mauvais login ou mot de passe !";
    }
}

?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
        <link rel="stylesheet" href="login.css">
    </head>
    <body onLoad="document.fo.login.focus()">
        <h1><img src="Cat.gif">Authentification<img src="Cat.gif"></h1>
        <div class="erreur"><?php echo $erreur ?>
        <form name="form" action="" method="post">
            <table>
                <tr>
                    <td>
                        <img src="UWU.gif">
                    </td>
                    <td>
                        <input type="text" name="pseudo" placeholder="Pseudo">
                        <input type="text" name="mdp" placeholder="Mot de passe">
                        <input type="submit" name="valider" value="S'authentifier">
                    </td>
                </tr>
            </table>        
            <br><a href="inscription.php">Créer votre compte</a>
        </form>
        </div>
    </body>
</html>