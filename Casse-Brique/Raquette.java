

import java.awt.*;

/*
 * ==============================================================================
 * BTS SIO 2 SLAM
 * ------------------------------------------------------------------------------
 * => Gestion des mouvements de la raquette
 * ==============================================================================
 */
/**
 * Classe représentant une raquette du jeu
 */
public class Raquette
    {
    private static final int X = 120;        // position "x" de la raquette (fixe !)
    private static final int Y = 30;        // position "y" de la raquette (fixe !)
    private static final int HEIGHT = 60;   // hauteur raquette
    private static final int WIDTH = 60;    // largeur raquette

    /**
     * Constructeur raquette
     *
     * @param game Espace de jeu (JPanel)
     * @param deb Pour debug
     */
    public Raquette(CasseBrique game, boolean deb)
        {


        }

    /**
     * Construction graphique de la raquette
     *
     * @param g
     */
    public void paint(Graphics2D g)
        {
        g.setColor(Color.white);
        g.fillRect(Raquette.X, Raquette.Y, Raquette.WIDTH, Raquette.HEIGHT);
        }




    /**
     * Récup position raquette
     *
     * @return Position raquette (coord. rect.)
     */
    public Rectangle getPositionRaquette()
        {
        return new Rectangle(Raquette.X, Raquette.Y, Raquette.WIDTH, Raquette.HEIGHT);
        }

    /**
     * Récup de la position "Y" de la raquette
     *
     * @return Pos Y
     */
    public int getY()
        {
        return Raquette.Y;
        }

    /**
     * Récup de la position "x" de la raquette
     *
     * @return Pos x
     */
    public int getX()
        {
        return Raquette.X;
        }


    public int getWidth()
        {
        return Raquette.WIDTH;
        }

        public int getHeight()
        {
        return Raquette.HEIGHT;
        }
    }
