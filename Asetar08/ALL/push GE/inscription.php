<?php

require_once("inc/init.inc.php");

if($_POST){
    
    $verif_caractere = preg_match('#^[a-zA-Z0-9._-]+$',$_POST['pseudo']);

    if(!$verif_caractere &&(strlen($_POST['pseudo']) < 1 || strlen($_POST['pseudo']) > 20)){
        $contenu .="<div class='erreur'>Le pseudo doit contenir entre 1 et 20 caractères.<br> Caractères accepté : Lettre de A à Z et chiffres de 0 à 9</div>";
    }
    else {
        $membre = executeRequete("SELECT * FROM membre WHERE pseudo='$_POST[pseudo]'");
        if($membre->num_rows > 0){
            $contenu .="<div class='erreur'>Pseudo indisponible. Veuillez en choisir un autre svp</div>";
        }
        else{
            $_POST['mdp'] = hash('sha256' ,"$_POST[mdp]");
            foreach($_POST as $indice => $valeur){
                $_POST[$indice] = htmlentities(addslashes($valeur));
            }
            executeRequete("INSERT INTO membre (pseudo, mdp, nom, prenom, email, civilite,{ ville, code_postal, adresse) VALUES ('$_POST[pseudo]', '$_POST[mdp]', '$_POST[nom]', '$_POST[prenom]', '$_POST[email]', '$_POST[civilite]', '$_POST[ville]', '$_POST[code_postal]', '$_POST[adresse]')");
            $contenu="";
        }
    }
}
