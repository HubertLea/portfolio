<link rel="stylesheet" href="resources/css/classe.css"> 

<body>

<h1>—————~ஜ Gestion des classes ஜ~—————</h1>

<div id="scroll_to_top">
    <a href="#top"><button id="topp" alt="Retourner en haut" >^</button></a>
</div>

<h4>Ici se trouve la possibilité de modifier chacune des classes, leurs élèves, et la possibilité d'ajouter une classe</h4><br>

<input type="submit" id="ajouterClasse" name="ajouterClasse" value="Ajouter une classe">

<form method="post">
        <br><br>

        <select name="classes" id="selectionneur">
            <?php
            include "$racine/config/conf.php";
            
            try {
                // Récupérer la liste des tables de classes existantes dans la base de données
                $tables_sql = "SHOW TABLES LIKE 'classe%'";
                $stmt_tables = $conn->query($tables_sql);
                $tables = $stmt_tables->fetchAll(PDO::FETCH_COLUMN);

                // Parcourir toutes les tables de classes
                foreach ($tables as $table) {
                    // Extraire le numéro de classe de chaque nom de table
                    $classe_number = str_replace("classe", "", $table);
                    echo '<option id="classe' . $classe_number . '" value="' . $table . '">Classe ' . $classe_number . '</option>';
                }
            } catch (PDOException $e) {
                // Gérer l'exception PDO
                echo "Erreur PDO : " . $e->getMessage();
            }
            ?>
        </select>

        <script src="resources/js/selecteur.js"></script>

        <?php
        include "controller/classesGestion.php";
        ?>
        </form>













</body>
</html>