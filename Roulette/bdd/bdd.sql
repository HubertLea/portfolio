-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Lun 06 Mars 2023 à 16:42
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `roulette`
--

-- --------------------------------------------------------

--
-- Structure de la table `liste`
--

CREATE TABLE IF NOT EXISTS `liste` (
  `id_liste` int(100) NOT NULL,
  `nom` text NOT NULL,
  `prenom` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `tirage`
--

CREATE TABLE IF NOT EXISTS `tirage` (
  `id_tirage` int(100) NOT NULL,
  `nom` text NOT NULL,
  `prenom` text NOT NULL,
  `presence` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `tirage`
--

INSERT INTO `tirage` (`id_tirage`, `nom`, `prenom`, `presence`) VALUES
(1, 'HUBERT', 'Lea', 'present'),
(2, 'HUBERTT', 'Leaa', 'present'),
(3, 'HUBERTTT', 'Leaaa', 'present');

-- --------------------------------------------------------

--
-- Structure de la table `tire`
--

CREATE TABLE IF NOT EXISTS `tire` (
  `id_tire` int(100) NOT NULL,
  `nom` text NOT NULL,
  `prenom` text NOT NULL,
  `note` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
