#!/usr/bin/php

<?php

function connexionSQL($serv,$user,$mdp,$bd) {
    /*Fonction qui permet d'établir une connexion sur le client MySQL*/
    $mysqli = mysqli_connect($serv, $user, $mdp, $bd);
    if (!$mysqli)
        {
        echo "=> Désolé, l'accès au SGBD n'est pas possible :(\n";
        }
    else {
        echo "Connexion réussie !\n";
        }
    return $mysqli;
}

function execReq ($sql,$conn) {
    /*Fonction qui exécute une requête SQL*/
    $result = mysqli_query($conn, $sql);
    return $result;
}

function extractReqSQL ($result,$conn) {
    /*Fonction qui extrait le contenu d'une requête SQL définie en paramètres*/
    if (mysqli_field_count($conn) === 0) {
        return "Requête vide";
    }
    else {
        $tab=[];
        foreach ($result as $ligne)  {
            $tab[]=$ligne;
            }
        return $tab;
    }
}

?>
