<!doctype html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>ASETAR08 Boutique</title>

    <link rel="stylesheet" href="css.css">
    

    <style>@import url('https://fonts.googleapis.com/css2?family=Hubballi&display=swap');</style>  <!--Trouvé sur google font pour changer de police de caractère-->
    
</head>

<body>



<nav>
                <h1 style="text-align:center">  ~ ❤️ ASETAR ❤️ ~  </h1><br>
            <ul>
                <li><a href="PageAcceuil.php">Acceuil</a></li>
                <li><a href="#">Blog</a></li>
                <li class="menu-deroulant">
                <a href="PageBoutique.php">Boutique</a>
                <ul class="sous-menu">
                    <li><a href="PageBoutique.php#Sweats">Sweats</a></li>
                    <li><a href="PageBoutique.php#Mugs">Mugs</a></li>
                    <li><a href="PageBoutique.php#Coussins">Coussins</a></li>
                    <li><a href="PageBoutique.php#Pochettes">Pochettes</a></li>
                    <li><a href="PageBoutique.php#Tshirts">Tshirts</a></li>
                    <li><a href="PagePanier.php">Panier</a></li>
                </ul>
                <li><a href="login.php">Se connecter</a></li>
                <li><a href="inscription.php">S'inscrire</a></li>
                <li><a href="">Profil</a></li>
            
        </nav>



    <br>
    <h3>🐇 Bienvenue dans la boutique Asetar ! 🐇</h3>
    <br>
    <h7 style="background-color:rgb(247, 159, 220)">Nos articles les plus vendus</h7>
    <br><br><br>
    <table>
        <tr>
            <td>
                <span class="info"><img src="Tshirt1.png" ></span>
                <div class="message"><p>20€<br>100% tissue<br>Taille disponible : XS, S, M, L, XL <br> en stock
                </p></div>
            </td>
            <td>
                <span class="info"><img src="Mug2.png" ></span>
                <div class="message"><p>35€<br>Verre trempé <br> en stock
                </p></div>
            </td>
            <td>
                <span class="info"><img src="Sweat2.png" ></span>
                <div class="message"><p>66€<br>100% tissue<br>Taille disponible : XS, S, M, L, XL <br> en stock
                </p></div>
            </td>
        </tr>
        <tr>
            <td>
                <input type="button" value="ajouter au panier">
            </td>
            <td>
                <input type="button" value="ajouter au panier">
            </td>
            <td>
                <input type="button" value="ajouter au panier">
            </td>
        </tr>
    </table>

    <br><br>
    <div id="Sweats">Nos Sweats</div><br>
    <table>
        <tr>
            <td>
                <span class="info"><img src="Sweat1.png" ></span>
                <div class="message"><p>66€<br>100% tissue<br>Taille disponible : XS, S, M, L, XL <br> en stock
                </p></div>
            </td>
            <td>
                <span class="info"><img src="Sweat2.png" ></span>
                <div class="message"><p>66€<br>100% tissue<br>Taille disponible : XS, S, M, L, XL <br> en stock
                </p></div>
            </td>
            <td>
                <span class="info"><img src="Sweat3.png" ></span>
                <div class="message"><p>70€<br>100% tissue<br>Taille disponible : XS, S, M, L, XL <br> en stock
                </p></div>
            </td>
        </tr>
        <tr>
            <td>
                <input type="button" value="ajouter au panier">
            </td>
            <td>
                <input type="button" value="ajouter au panier">
            </td>
            <td>
                <input type="button" value="ajouter au panier">
            </td>
        </tr>
    </table>

    <br>
    <div id="Mugs">Nos Mugs</div><br>
    <table>
        <tr>
            <td>
                <span class="info"><img src="Mug1.png" ></span>
                <div class="message"><p>36€<br>Verre trempé<br>en stock
                </p></div>
            </td>
            <td>
                <span class="info"><img src="Mug2.png" ></span>
                <div class="message"><p>35€<br>Verre trempé<br> en stock
                </p></div>
            </td>
        </tr>
        <tr>
            <td>
                <input type="button" value="ajouter au panier">
            </td>
            <td>
                <input type="button" value="ajouter au panier">
            </td>
        </tr>
    </table>

    <br>
    <div id="Coussins">Nos Coussins</div><br>
    <table>
        <tr>
            <td>
                <span class="info"><img src="Coussin.png" ></span>
                <div class="message"><p>85€<br>100% tissue<br>Taille disponible : XS, S, M, L, XL <br> en stock
                </p></div>
            </td>
        </tr>
        <tr>
            <td>
                <input type="button" value="ajouter au panier">
            </td>
        </tr>
    </table>

    <br>
    <div id="Pochettes">Nos Pochettes</div><br>
    <table>
        <tr>
            <td>
                <span class="info"><img src="Pochette1.png" ></span>
                <div class="message"><p>29€<br>100% tissue<br> en stock
                </p></div>
            </td>
            <td>
                <span class="info"><img src="Pochette2.png" ></span>
                <div class="message"><p>30€<br>100% tissue<br>en stock
                </p></div>
            </td>
        </tr>
        <tr>
            <td>
                <input type="button" value="ajouter au panier">
            </td>
            <td>
                <input type="button" value="ajouter au panier">
            </td>
        </tr>
    </table>

    <br>
    <div id="Tshirts">Nos Tshirts</div><br>
    <table>
        <tr>
            <td>
                <span class="info"><img src="Tshirt1.png" ></span>
                <div class="message"><p>20€<br>100% tissue<br>Taille disponible : XS, S, M, L, XL <br> en stock
                </p></div>
            </td>
            <td>
                <span class="info"><img src="Tshirt2.png" ></span>
                <div class="message"><p>25€<br>100% tissue<br>Taille disponible : XS, S, M, L, XL <br> en stock
                </p></div>
            </td>
            <td>
                <span class="info"><img src="Tshirt3.png" ></span>
                <div class="message"><p>22€<br>100% tissue<br>Taille disponible : XS, S, M, L, XL <br> en stock
                </p></div>
            </td>
            <td>
                <span class="info"><img src="Debardeur.png" ></span>
                <div class="message"><p>20€<br>100% tissue<br>Taille disponible : XS, S, M, L, XL <br> en stock
                </p></div>
            </td>
        </tr>
        <tr>
            <td>
                <input type="button" value="ajouter au panier">
            </td>
            <td>
                <input type="button" value="ajouter au panier">
            </td>
            <td>
                <input type="button" value="ajouter au panier">
            </td>
            <td>
                <input type="button" value="ajouter au panier">
            </td>
        </tr>
    </table><br><br>
</body>

<footer id="footer">

                <div>
                    <ul style="display: flex; margin:auto; gap: 32%;">
                        <li style="display: flex;text-shadow:3px 3px 3px black ;">
                            Conditions:
                            <a href="https://paca.dreets.gouv.fr/sites/paca.dreets.gouv.fr/IMG/pdf/cgu__demat_d5b9d.pdf"> CGU</a>
                            / 
                            <a href="https://www.legalplace.fr/guides/conditions-generales-vente/"> CGV</a>
                        </li>
                        <li style="display: flex;text-shadow:3px 3px 3px black ;">
                            Réglementation:
                             <a href="https://www.cnil.fr/fr/conformite-rgpd-information-des-personnes-et-transparence"> RGPD</a>
                        </li>
                        <li style="display: flex;text-shadow:3px 3px 3px black ;">
                            Nous contacter:  
                             <a href="https://trello.com/b/gE1eG3Mi">  Trello@issou.gouv</a>
                        </li>
                    </ul>
                </div>
                <div id="copyright">
                    © 2022 Team3 Copyright, S9, Lycée Monge. All Rights Reserved
                </div>
</footer>
</html>
