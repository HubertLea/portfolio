<?php
function routeur($action){
    $lesActions = array();
	$lesActions["defaut"] = "controllerAccueil.php";
    $lesActions["accueil"] = "controllerAccueil.php";
    $lesActions["tirage"] = "controllerTirage.php";
    $lesActions["classes"] = "controllerClasses.php";

    
    if (array_key_exists ( $action , $lesActions )){
        return $lesActions[$action];
    }
    else{
        return $lesActions["defaut"];
    }

}

?>