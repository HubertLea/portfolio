<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="style.css" />
    </head>
    <body>
        <?php
        require('config.php');
        if (isset($_REQUEST['pseudo']) and isset($_REQUEST['mail'])) {

            $pseudo = stripslashes($_REQUEST['pseudo']);
            $pseudo = mysqli_real_escape_string($conn, $pseudo);

            $mail = stripslashes($_REQUEST['mail']);
            $mail = mysqli_real_escape_string($conn, $mail);

            $mdp = stripslashes($_REQUEST['mdp']);
            $mdp = mysqli_real_escape_string($conn, $mdp);

            $query = "INSERT into UTILISATEUR (ID_uti, pseudo, mail, mdp) VALUES (1,'$pseudo', '$mail','". hash('sha256', $mdp)."');";
            
            $res = mysqli_query($conn, $query);
            if($res) {
                echo "<div class='sucess'>
                        <h3>Vous êtes inscrit avec succès</h3>
                        <p>Cliquez ici pour vous <a href='login.php'>connecter</a></p>
                      </div>";                        
            }
        }
        else {
            ?>
            <form class="box" action="" method="post">
                <h1 class="box_logo box-title"><a href=""></a></h1>
                <h1 class="box-title">S'inscrire</h1>
                <input type="text" class="box-input" name="pseudo" placeholder="pseudo" required />
                <input type="text" class="box-input" name="mail" placeholder="mail" required />
                <input type="password" class="box-input" name="password" placeholder="Mot de passe" required />
                <input type="submit" class="box-button" name="submit" value="S'incrire" />
                <p class="box-register">Déjà inscrit ? <a href="login.php">Connectez-vous ici</a></p>
            </form>
            <?php  
        }
         
        ?>
    </body>
</html>