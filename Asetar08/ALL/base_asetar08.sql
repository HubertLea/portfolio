DROP DATABASE IF EXISTS ASETAR08;
CREATE DATABASE ASETAR08;

CREATE USER IF NOT EXISTS 'TEAM3'@'localhost' IDENTIFIED BY 'TEAM3';
GRANT ALL PRIVILEGES ON ASETAR08.* TO 'TEAM3'@'localhost' WITH GRANT OPTION ; 

USE ASETAR08;

CREATE TABLE COMPTABILITE (
    ID_compta   INTEGER NOT NULL,
    nom         VARCHAR (35),
    dte         DATE,
    quantite    INTEGER NOT NULL,
    prix_achat  FLOAT NOT NULL,
    prix_vente  FLOAT NOT NULL,
    PRIMARY KEY (ID_compta)
    )  ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
    
CREATE TABLE STOCK (
    ID_art      INTEGER NOT NULL,
    nom_art     VARCHAR(35),
    quantite    INTEGER NOT NULL,
    prix        FLOAT NOT NULL,
    PRIMARY KEY (ID_art)
    )  ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE IMAGES (
    ID_img      INTEGER NOT NULL,
    src         VARCHAR (255),
    description VARCHAR (255),
    PRIMARY KEY (ID_img)
    )  ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
    
CREATE TABLE PUBLICATIONS (
    ID_pub      INTEGER NOT NULL,
    nom         VARCHAR (255),
    dte         date,
    genre       VARCHAR (255),
    etat        VARCHAR (255),
    comm        VARCHAR (255),
    avis        VARCHAR (255),
    PRIMARY KEY (ID_pub)
    )  ENGINE=InnoDB DEFAULT CHARSET=utf8mb4; 

CREATE TABLE UTILISATEUR (
    ID_uti      INTEGER NOT NULL,
    mail        VARCHAR (255),
    mdp         VARCHAR (255),
    nom         VARCHAR (255),
    age         VARCHAR (255),
    rôle        VARCHAR (255),
    pseudo      VARCHAR (255),
    statut      VARCHAR (255),
    bio         VARCHAR (255),
    PRIMARY KEY (ID_uti)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE CONVERSATION (
    ID_conv     INTEGER NOT NULL,
    nom         VARCHAR (255),
    membres     VARCHAR (255),
    PRIMARY KEY (ID_conv)
    )  ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

    
CREATE TABLE INCLURE (
    img      INTEGER NOT NULL ,
    conv     INTEGER NOT NULL,
    pub      INTEGER NOT NULL,
    FOREIGN KEY (pub) REFERENCES PUBLICATIONS (ID_pub),
    FOREIGN KEY (conv) REFERENCES CONVERSATION (ID_conv),
    FOREIGN KEY (img) REFERENCES IMAGES (ID_img)  
    )  ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
    
INSERT INTO IMAGES VALUES (1, "coeurVide.png","publication non likée");
INSERT INTO IMAGES VALUES (2, "coeurPlein.png","publication likée");
INSERT INTO IMAGES VALUES (3, "Coms.png","espace commentaire");
INSERT INTO IMAGES VALUES (4, "favorisVide.png", "publication non présente dans les archives");
INSERT INTO IMAGES VALUES (5, "favorisPlein.png", "publication présente dans les archives");
INSERT INTO IMAGES VALUES (6, "logo.png","Logo");
INSERT INTO IMAGES VALUES (7, "Coussin.png","photo du coussin1 de la boutique");
INSERT INTO IMAGES VALUES (8, "Debardeur.png","photo du Debardeur1 de la boutique");
INSERT INTO IMAGES VALUES (9, "Mug1.png","photo du mug1 de la boutique");
INSERT INTO IMAGES VALUES (10,"Mug2", "photo du mug2 de la boutique");
INSERT INTO IMAGES VALUES (11,"Pochette1.png", "photo de la pochette1 de la boutique");
INSERT INTO IMAGES VALUES (12,"Pochette2.png", "photo de la pochette2 de la boutique");
INSERT INTO IMAGES VALUES (13,"Sweat1.png", "photo du sweat1 de la boutique");
INSERT INTO IMAGES VALUES (14,"Sweat2.png", "photo du sweat2 de la boutique");
INSERT INTO IMAGES VALUES (15,"Sweat3.png", "photo du sweat3 de la boutique");
INSERT INTO IMAGES VALUES (16,"Tshirt1.png", "photo du Tshirt1 de la boutique");
INSERT INTO IMAGES VALUES (17,"Tshirt2.png", "photo du Tshirt2 de la boutique");
INSERT INTO IMAGES VALUES (18,"Tshirt3.png", "photo du Tshirt3 de la boutique");
INSERT INTO IMAGES VALUES (19, "Actualite1", "photo de l'actualité");

