<link rel="stylesheet" href="resources/css/tirage.css"> 

<body>

<div id="scroll_to_top">
    <a href="#top"><button id="topp" alt="Retourner en haut" >^</button></a>
</div>


        <h1>—————~ஜ Tirage au sort ஜ~—————</h1>

        <form method="post">
        <br><br>

        <select name="classes" id="selectionneur">
            <?php
            include "$racine/config/conf.php";
            
            try {
                // Récupérer la liste des tables de classes existantes dans la base de données
                $tables_sql = "SHOW TABLES LIKE 'classe%'";
                $stmt_tables = $conn->query($tables_sql);
                $tables = $stmt_tables->fetchAll(PDO::FETCH_COLUMN);

                // Parcourir toutes les tables de classes
                foreach ($tables as $table) {
                    // Extraire le numéro de classe de chaque nom de table
                    $classe_number = str_replace("classe", "", $table);
                    echo '<option id="classe' . $classe_number . '" value="' . $table . '">Classe ' . $classe_number . '</option>';
                }
            } catch (PDOException $e) {
                // Gérer l'exception PDO
                echo "Erreur PDO : " . $e->getMessage();
            }
            ?>
        </select>

        <script src="resources/js/selecteur.js"></script>

        <br><br><br>
        
        <input type="submit" id="melange" name="melange" value="Tirer au sort">

        <input type="submit" id="melange" name="melangeabs" value="Tirer un absent">

        <br><br>
        
        <input type="submit" id="reseter" name="reseter" value="Réinitialiser les passages">

        <input type="submit" id="reseterTout" name="reseterTout" value="Tout réinitialiser">

        <br><br>


<?php

include "controller/tirage_sort.php";
include "controller/tirage_abs.php";
include "controller/notes.php";
include "controller/reset_passage.php";
include "controller/reset_passage_note.php";

?>

<br><br>
<CENTER>
<table>
    <tr>
        <td style="border-top:none;"><h3>Absences :</h3></td>
        <td style="border-top:none;">
        <?php
            include "controller/absences.php";
        ?>
        </td>
    </tr>
    <tr>
        <td><h3>Personnes restantes :</h3></td>
        <td>
        <?php
            include "controller/personnes_restantes.php";
        ?>
        </td>
    </tr>
    <tr>
        <td style="border-bottom:none;"><h3>Personnes déjà notées :</h3></td>
        <td style="border-bottom:none;">
        <?php
            include "controller/personnes_tirees.php";
        ?>
        </td>
    </tr>
</table>
</CENTER>

</form>







</body>
</html>