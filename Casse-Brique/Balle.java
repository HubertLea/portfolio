import java.awt.*;
import java.util.concurrent.ThreadLocalRandom;

/*
 * ==============================================================================
 * BTS SIO 2 SLAM
 * ------------------------------------------------------------------------------
 * => Gestion des mouvements de la balle
 * ==============================================================================
 */
/**
 * Classe représentant la balle
 */
public class Balle
    {
    private static final int DIAMETER = 50;   // diamètre de la balle
    private final CasseBrique brique;          // lien vers la fenêtre principale
    private int x;                            // position "x" de la balle
    private int y;                            // position "y" de la balle
    private int xspd;                         // Vitesse/déplacement "x" de la balle
    private int yspd;                         // Vitesse/déplacement "y" de la balle
    private boolean DEBUG = false;            // Pour affichage debug ...
    private String name;

    /**
     * Constructeur de la balle
     *
     * @param game Espace de jeu (JPanel)
     * @param deb Pour debug
     */
    public Balle(CasseBrique game, boolean deb)
        {
        this.brique = game;
        this.resetPosBalle();
        this.DEBUG = deb;
        }

    private void resetPosBalle()
        {
        Dimension dim = this.brique.getPreferredSize();
        this.x = dim.width / 2;
        this.y = dim.height / 2;
        this.xspd = 1;
        this.yspd = 1;
        }

    /**
     * Déplacement balle ...
     */

    void move(Balle balle)
        {
        // Axe X vers 0 : Bord gauche du panel
        if (this.x + this.xspd < 0)
            {
            this.xspd = brique.getSpeed();
            }
        // Axe X vers x max : Bord droit du panel
        if (this.x + this.xspd > brique.getWidth() - Balle.DIAMETER)
            {
            this.xspd = -brique.getSpeed();
            }
        // Axe Y vers 0 : Bord haut du panel
        if (this.y + this.yspd < 0)
            {
            this.yspd = brique.getSpeed();
            }
        // Axe Y vers y max : Bord bas du panel
        if (this.y + this.yspd > brique.getHeight() - Balle.DIAMETER)
            {
            this.yspd = -brique.getSpeed();
            }

        //--------------------------------------------------------------
        this.x += this.xspd;
        this.y += this.yspd;
        if (collision())
            {
              // Axe X vers y max : Bord gauche de la raquette
              if ((this.x + (Balle.DIAMETER / 2)) > brique.getPosXRaquette())
                  {
                  this.xspd = brique.getSpeed();
                  }
              // Axe X vers x max : Bord droit de la raquette
              if ((this.x + (Balle.DIAMETER / 2)) < (brique.getPosXRaquette() + brique.getPosWidthRaquette()))
                  {
                  this.xspd = -brique.getSpeed();
                  }
              // Axe Y vers 0 : Bord haut de la raquette
              if ((this.y + (Balle.DIAMETER / 2))> brique.getPosYRaquette())
                  {
                  this.yspd = brique.getSpeed();
                  }
              // Axe Y vers y max : Bord bas du panel
              if ((this.y + (Balle.DIAMETER / 2)) < (brique.getPosYRaquette() + brique.getPosHeightRaquette()))
                  {
                  this.yspd = -brique.getSpeed();
                  }
                  this.x += this.xspd;
                  this.y += this.yspd;
            }
        if (collisionBalle(balle))
            {
              // Axe X vers y max : Bord gauche de la balle
              if ((this.x + (this.DIAMETER / 2)) > balle.x)
                  {
                  this.xspd = brique.getSpeed();
                  }
              // Axe X vers x max : Bord droit de la balle
              if ((this.x + (this.DIAMETER / 2)) < (balle.x + balle.DIAMETER))
                  {
                  this.xspd = -brique.getSpeed();
                  }
              // Axe Y vers 0 : Bord haut de la balle
              if ((this.y + (this.DIAMETER / 2))> balle.y)
                  {
                  this.yspd = brique.getSpeed();
                  }
              // Axe Y vers y max : Bord bas du balle
              if ((this.y + (this.DIAMETER / 2)) < (balle.y + balle.DIAMETER))
                  {
                  this.yspd = -brique.getSpeed();
                  }
                  this.x += this.xspd;
                  this.y += this.yspd;
            }
        if (DEBUG)
            {
            System.out.println("=> Balle" 
                + this.name
                +" (x/y) : "
                + this.x
                + "/"
                + this.y
                + " [speed : "
                + this.brique.getSpeed()
                + "] [RUG : "
              //  + (int) rugositeAleatoire
                + "] "
                + "(taille écran x/y : "
                + this.brique.getWidth()
                + "/"
                + this.brique.getHeight()
                + ") (Diamètre balle : "
                + Balle.DIAMETER
                + ")");
            }
        }

    /**
     * Construction graphique de la balle
     *
     * @param g Objet graphique (canvas)
     */
    public void paint(Graphics2D g)
        {
        g.setColor(Color.red);
        g.fillOval(this.x, this.y, Balle.DIAMETER, Balle.DIAMETER);
        }

    /**
     * récup. position actuelle de la balle
     *
     * @return Position de la balle (coord d'un rect.)
     */
    public Rectangle getPositionBalle()
        {
        // Pourquoi retourner un objet "Rectangle", puique nous avons
        // affaire ici à une balle ?
        return new Rectangle(this.x, this.y, Balle.DIAMETER, Balle.DIAMETER);
        }

    /**
     * Test si la balle entre en collision avec la raquette ...
     *
     * @return Collision balle/raquette (T/F)
     */
    private boolean collision()
        {
        return brique.getPositionRaquette().intersects(this.getPositionBalle());
        }

    private boolean collisionBalle(Balle balle)
        {
        return getPositionBalle2(balle).intersects(this.getPositionBalle());
        }

    public Rectangle getPositionBalle2(Balle balle)
        {
        return new Rectangle(balle.x, balle.y, balle.DIAMETER, balle.DIAMETER);
        }
    

    /**
     * Recup. vitesse x de la balle
     *
     * @return Vitesse 'x' de la balle
     */
    public int getXspd()
        {
        return xspd;
        }

    /**
     *
     * Mise à jour de la vitesse 'x' de la balle
     *
     * @param xspd Nouvelle vitesse 'x' de la balle
     */
    public void setXspd(int xspd)
        {
        this.xspd = xspd;
        }

    /**
     * Recup. vitesse y de la balle
     *
     * @return Vitesse 'y' de la balle
     */
    public int getYspd()
        {
        return yspd;
        }

    /**
     * Mise à jour de la vitesse 'y' de la balle
     *
     * @param yspd Nouvelle vitesse 'y' de la balle
     *
     */
    public void setYspd(int yspd)
        {
        this.yspd = yspd;
        }
    

    public void setx(int posx){
        this.x = posx;
    }

    public void sety(int posy){
        this.y = posy;
    }

    public void name(String nom){
        this.name = nom;
    }

    public int getx(){
        return x;
    }

    public int gety(){
        return y;
    }

    public int getdiametre(){
        return DIAMETER;
    }
}
